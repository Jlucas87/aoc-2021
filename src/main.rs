mod aoc2021;
mod aoc2023;

fn main() {
    // Run the 2021 module
    // aoc2021::run_days();

    // Run the 2023 module
    aoc2023::run_days();
}