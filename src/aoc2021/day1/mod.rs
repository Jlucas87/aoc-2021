use std::fs;
use std::time::{Instant};

pub fn run_day_1_1(filename: String) {
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<i32> = contents.split("\n")
        .map(|x| { x.parse().unwrap() })
        .collect();

    let mut count = 0;
    for i in 1 .. input_values.len() {
        if input_values[i] > input_values[i - 1] {
            count = count + 1;
        }
    }

    println!("{}", count);
}


pub fn run_day_1_2(filename: String) {
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<i32> = contents.split("\n")
        .map(|x| { x.parse().unwrap() })
        .collect();

    let mut count = 0;
    for i in 3 .. input_values.len() {
        let sum0 = input_values[i - 1] + input_values[i - 2] + input_values[i - 3];
        let sum1 = input_values[i] + input_values[i - 1] + input_values[i - 2];
        if sum1 > sum0 {
            count += 1;
        }
    }

    println!("{}", count);
}
