use std::fs;
use std::time::{Instant};


pub fn run_day_7_1(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day6 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut input_values: Vec<i32> = contents.split(",")
        .map(|x| { x.parse().unwrap() })
        .collect();
    input_values.sort();
    println!("{:?}", input_values.len());
    let mut median = 0;
    if input_values.len() as i32 % 2 == 0 {
        let half = input_values.len() / 2;
        median = (input_values[half - 1] + input_values[half]) / 2;
    } else {
        median = (input_values.len() as i32 + 1) / 2;
    }

    println!("{}", median);

    // Iterate through list and determine total fuel consumption
    let mut total_fuel = 0;
    for crab_submarine in input_values {
        let fuel_for_crab = (crab_submarine - median).abs();
        total_fuel += fuel_for_crab;
    }

    println!("{}", total_fuel);
}

// Utilize Gauss's rule to find the most efficient midpoint.
// (n / 2) * (n + 1)
pub fn run_day_7_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day6 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut input_values: Vec<i32> = contents.split(",")
        .map(|x| { x.parse().unwrap() })
        .collect();
    input_values.sort();
    let _length = input_values.len() as i32;

    let mut fuel_total = 0;
    let mut smallest_total = 2147483647;
    for pos in input_values[0] .. input_values[&input_values.len() - 1] {
        for value in &input_values {
            fuel_total += fuel_consumption(&value, pos);
        }
        println!("{} {}", pos, fuel_total);
        if fuel_total < smallest_total {
            smallest_total = fuel_total;
        }
        fuel_total = 0;
    }
    println!("{}", smallest_total);
}

fn gauss_calc (n: f32) -> i32 {
    return ((n / 2.0) * (n + 1.0)) as i32;
}

fn fuel_consumption (pos: &i32, avg: i32) -> i32 {
    let dist = (pos - avg).abs();
    return gauss_calc(dist as f32);
}