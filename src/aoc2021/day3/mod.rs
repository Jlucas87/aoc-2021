use std::fs;
use std::time::{Instant};

pub fn run_day_3_1(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // Determine how many entries there are and how many digits are needed for gamma
    // and epsilon
    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let entries = input_values.len();
    let digits = input_values[0].len();
    println!("Entries: {}, digits: {}", entries, digits);
    let mut counts = Vec::with_capacity(digits);
    for _i in 0 .. digits {
        counts.push(0);
    }

    // Determine how many 1s there are per line in the data
    for entry in input_values {
        for i in 0 .. digits {
            let digit = char::to_string(&entry.chars().nth(i).unwrap());
            if digit == "1" {
                counts[i] = counts[i] + 1;
            }
        }
    }

    // Create the gamma and epsilon values by seeing if we need a 1 or a 0 for each digit.
    let mut gamma = "".to_owned();
    let mut epsilon = "".to_owned();
    for count in &counts {
        if count > &(&entries/2) {
            gamma.push_str("1");
            epsilon.push_str("0");
        } else {
            gamma.push_str("0");
            epsilon.push_str("1");
        }
    }

    // Convert the binary values for gamma and epsilon to decimal values.
    println!("{} {}", gamma, epsilon);
    let intval_gamma = isize::from_str_radix(&gamma, 2).unwrap();
    let intval_epsilon = isize::from_str_radix(&epsilon, 2).unwrap();

    // Display and calculate the final solution by getting total power consumption of the submarine.
    println!("{}, {}", intval_gamma, intval_epsilon);
    println!("{:?}", intval_gamma * intval_epsilon);
}

pub fn run_day_3_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // Determine how many entries there are and how many digits are needed for gamma
    // and epsilon
    let mut oxygen_values: Vec<&str> = contents.split("\n")
        .collect();
    let mut co2_values = oxygen_values.clone();
    let _entries = oxygen_values.len();
    let digits = oxygen_values[0].len();
    let mut counts = Vec::with_capacity(digits);
    for _i in 0 .. digits {
        counts.push(0);
    }

    // Oxygen Generator = most common bit
    // CO2 Scrubber = least common bit
    for i in 0 .. digits {
        let count = find_1_bit_count_in_pos(&oxygen_values, i);
        if oxygen_values.len() < 2 {
            break;
        }
        println!("{} {}", count, oxygen_values.len() - count);
        if count >= oxygen_values.len() - count {
            oxygen_values.retain(|&item| char::to_string(&item.chars().nth(i).unwrap()) == "1");
        } else {
            oxygen_values.retain(|&item| char::to_string(&item.chars().nth(i).unwrap()) == "0");
        }
    }

    println!("{:?}", oxygen_values);

    for i in 0 .. digits {
        let count = find_0_bit_count_in_pos(&co2_values, i);
        if co2_values.len() < 2 {
            break;
        }
        println!("{} {}", count, co2_values.len() - count);
        if count <= co2_values.len() - count {
            co2_values.retain(|&item| char::to_string(&item.chars().nth(i).unwrap()) == "0");
        } else {
            co2_values.retain(|&item| char::to_string(&item.chars().nth(i).unwrap()) == "1");
        }
    }

    println!("{:?}", co2_values);

    let intval_co2 = isize::from_str_radix(&co2_values[0], 2).unwrap();
    let intval_oxygen = isize::from_str_radix(&oxygen_values[0], 2).unwrap();
    println!("{}, {}", intval_co2, intval_oxygen);
    println!("{}", intval_co2 * intval_oxygen);
}

fn find_1_bit_count_in_pos (lines: &Vec<&str>, pos: usize) -> usize {
    let _len = lines.len();
    let mut count: usize = 0;
    for entry in lines {
        let digit = char::to_string(&entry.chars().nth(pos).unwrap());
        if digit == "1" {
            count += 1;
        }
    }

    return count;
}

fn find_0_bit_count_in_pos (lines: &Vec<&str>, pos: usize) -> usize {
    let _len = lines.len();
    let mut count: usize = 0;
    for entry in lines {
        let digit = char::to_string(&entry.chars().nth(pos).unwrap());
        if digit == "0" {
            count += 1;
        }
    }

    return count;
}