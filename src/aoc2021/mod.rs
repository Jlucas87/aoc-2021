mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;

pub fn run_days() {
    // Day 1
    // println!("Day 1, part1");
    // day1::run_day_1_1(String::from("./src/day1/input.txt"));
    //
    // println!("Day 1, part2");
    // day1::run_day_1_2(String::from("./src/day1/input.txt"));

    // Day 2
    // println!("Day 2, part1");
    // day2::run_day_2_1(String::from("./src/day2/input.txt"));
    //
    // println!("Day2, part2");
    // day2::run_day_2_2(String::from("./src/day2/input.txt"));

    // Day 3
    // println!("Day 3, part1");
    // day3::run_day_3_1(String::from("./src/day3/input.txt"));

    // println!("Day3, part2");
    // day3::run_day_3_2(String::from("./src/day3/input.txt"));

    // Day 4
    // println!("Day 4, part1");
    // day4::run_day_4_1(String::from("./src/day4/input.txt"));
    //
    // println!("Day 4, part2");
    // day4::run_day_4_2(String::from("./src/day4/input.txt"));

    // Day 5
    // println!("Day 5, part1");
    // day5::run_day_5_1(String::from("./src/day5/input.txt"));
    //
    // println!("Day 5, part2");
    // day5::run_day_5_2(String::from("./src/day5/input.txt"));

    // Day 6
    // println!("Day 6, part1");
    // day6::run_day_6(String::from("./src/day6/input.txt"), 80);
    //
    // println!("Day 6, part2");
    // day6::run_day_6(String::from("./src/day6/input.txt"), 256);

    // Day 7
    // println!("Day 7, part1");
    // day7::run_day_7_1(String::from("./src/day7/input.txt"));
    //
    // println!("Day 7, part2");
    // day7::run_day_7_2(String::from("./src/day7/input.txt"));

    // Day 8
    // println!("Day 8, part1");
    // day8::run_day_8_1(String::from("./src/day8/input.txt"));

    // println!("Day 8, part2");
    // day8::run_day_8_2(String::from("./src/day8/input.txt"));

    // Day 9
    // println!("Day 9, part1");
    // day9::run_day_9_1(String::from("./src/day9/input.txt"));

    // println!("Day 9, part2");
    // day9::run_day_9_2(String::from("./src/day9/input.txt"));

    // Day 10
    // println!("Day 10, part1");
    // day10::run_day_10_1(String::from("./src/day10/input.txt"));
    //
    // println!("Day 10, part2");
    // day10::run_day_10_2(String::from("./src/day10/input.txt"));

    // Day 11
    // println!("Day 11, part1");
    // day11::run_day_11_1(String::from("./src/day11/input.txt"), 100);

    // println!("Day 11, part2");
    // day11::run_day_11_2(String::from("./src/day11/input.txt"));

    // Day 12
    println!("Day 12, part1");
    day12::run_day_12_1(String::from("./src/aoc2021/day12/input.txt"));

    // println!("Day 12, part2");
    // day12::run_day_12_2(String::from("./src/day12/input.txt"));
}
