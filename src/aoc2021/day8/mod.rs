use std::fs;
use std::time::{Instant};
use std::collections::HashMap;

pub fn run_day_8_1(filename: String) {
    let _day8_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    /* Note on digits:
     * 1 - 2 segments
     * 4 - 4 segments
     * 7 - 3 segments
     * 8 - 7 segments
     */

    let mut total = 0;
    let lines: Vec<&str> = contents.split("\n")
         .collect();
    // println!("{:?}", lines);

    for line in lines {
        let split_line: Vec<&str> = line.split(" | ").collect();
        let right_side: Vec<&str> = split_line[1].split(" ").collect();
        for item in right_side {
            let input: i32 = item.len() as i32;
            match input {
                2 | 3 | 4 | 7 => total += 1,
                _ => ()
            }
        }
    }

    println!("{}", total)
}
/*
 * Find 1, 4, 7, and 8 first. Add to hash map, remove from vector.
 * Find 9 from 4.
 */
pub fn run_day_8_2(filename: String) {
    let _day8_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut total = 0;
    let lines: Vec<&str> = contents.split("\n")
         .collect();

    for line in lines {
        let split_line: Vec<&str> = line.split(" | ").collect();
        let left_side: Vec<&str> = split_line[0].split(" ").collect();
        let right_side: Vec<&str> = split_line[1].split(" ").collect();

        let mut numbers: HashMap<i32, &str> = HashMap::new();
        let mut patterns: HashMap<&str, i32> = HashMap::new();

        // --- Create pattern map --- //
        // Find 1, 4, 7, and 8
        for item in &left_side {
            let length: i32 = item.len() as i32;
            match length {
                2 => { numbers.insert(1, item); patterns.insert(item, 1); },
                3 => { numbers.insert(7, item); patterns.insert(item, 7); },
                4 => { numbers.insert(4, item); patterns.insert(item, 4); },
                7 => { numbers.insert(8, item); patterns.insert(item, 8); },
                _ => ()
            }
        }

        // Find 9 from 4, find 3 from 1
        for item in &left_side {
            let is_nine = find_chars_in_string(item, numbers.get(&4).unwrap().chars().collect(), 6);
            if is_nine {
                numbers.insert(9, item);
                patterns.insert(item, 9);
            }
            let is_three = find_chars_in_string(item, numbers.get(&1).unwrap().chars().collect(), 5);
            if is_three {
                numbers.insert(3, item);
                patterns.insert(item, 3);
            }
        }

        // Find 0 from 1 and ignore 9
        for item in &left_side {
            let is_zero = find_chars_in_string(item, numbers.get(&1).unwrap().chars().collect(), 6);
            if is_zero && numbers.get(&9).unwrap() != item {
                numbers.insert(0, item);
                patterns.insert(item, 0);
            }
        }

        // Find 6 through process of elimination
        for item in &left_side {
            if item.len() as i32 == 6 && numbers.get(&9).unwrap() != item && numbers.get(&0).unwrap() != item {
                numbers.insert(6, item);
                patterns.insert(item, 6);
            }
        }

        // Find 5 from 6
        for item in &left_side {
            if numbers.get(&6).unwrap() != item {
                let is_five = find_chars_in_string(numbers.get(&6).unwrap(), item.chars().collect(), 6);
                if is_five {
                    numbers.insert(5, item);
                    patterns.insert(item, 5);
                }
            }
        }

        // Find 2 as last remaining value
        for item in &left_side {
            if !patterns.contains_key(item) {
                numbers.insert(2, item);
                patterns.insert(item, 2);
                break;
            }
        }

        // --- Determine displayed digits --- //
        let mut line_string: String = "".to_owned();
        for item in &right_side {
            for (key, val) in patterns.iter() {
                if key.len() as i32 == item.len() as i32 {
                    if check_chars_all_in_string(item, key.chars().collect()) {
                        line_string = format!("{}{}", line_string, val.to_string());
                    }
                }

            }
        }
        total = total + line_string.parse::<i32>().unwrap();
    }

    println!("Total of lines: {}", total)
}

fn find_chars_in_string (input: &str, chars: Vec<char>, char_len: i32) -> bool {
    let mut result = true;
    let length: i32 = input.len() as i32;
    if length == char_len {
        for c in chars {
            if input.find(c) == None {
                result = false;
            }
        }
    } else {
        result = false;
    }

    return result;
}

fn check_chars_all_in_string (input: &str, chars: Vec<char>) -> bool {
    let mut result = true;
    for c in chars {
        if input.find(c) == None {
            result = false;
        }
    }

    return result;
}