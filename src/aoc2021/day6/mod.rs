use std::fs;
use std::time::{Instant};
use std::collections::HashMap;

pub fn run_day_6(filename: String, iterations: i32) {
    let _depth = 0;
    let _distance = 0;
    let day6 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // Determine how many entries there are and how many digits are needed for gamma
    // and epsilon
    let input_values: Vec<&str> = contents.split(",")
        .collect();

    // Create a datastructure to track all of the fish
    let mut fish: HashMap<i32, i128> = HashMap::new();
    for timer_val in 0 .. 9 {
        fish.insert(timer_val, 0i128);
    }

    // Create initial map of the fish
    for val in input_values {
        let int_val = val.parse::<i32>().unwrap();
        fish.insert(int_val, fish.get(&int_val).unwrap() + &1i128);
    }

    println!("{:?}", fish);

    // Start the great reproduction cycle
    let mut zeroes: i128 = 0;
    for _i in 0 .. iterations {
        for timer_val in 0 .. 9 {
            if timer_val == 0 {
                zeroes = fish.get(&0).unwrap().clone();
            } else {
                fish.insert(timer_val - 1, fish.get(&timer_val).unwrap().clone());
            }
        }
        fish.insert(8, zeroes);
        fish.insert(6, fish.get(&6).unwrap() + zeroes);
    }
    println!("{:?}", fish);
    count_total_fish(&fish);
    println!("{}", format!("{}{}", "Microseconds: ", day6.elapsed().as_micros()));
}

fn count_total_fish (hash_map: &HashMap<i32, i128>) {
    let mut sum: i128 = 0;
    for (_key, val) in hash_map.iter() {
        sum += val
    }

    println!("{}", sum);
}