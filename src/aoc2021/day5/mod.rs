use std::fs;
use std::time::{Instant};
use std::collections::HashMap;

pub fn run_day_5_1(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day5_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // Determine how many entries there are and how many digits are needed for gamma
    // and epsilon
    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let mut points = HashMap::new();

    for line in input_values {
        let coords: Vec<&str> = line.split(" -> ").collect();
        let coord1: Vec<&str> = coords[0].split(",").collect();
        let coord2: Vec<&str> = coords[1].split(",").collect();
        if coord1[0] == coord2[0] {
            let mut y1 = coord1[1].parse::<i32>().unwrap();
            let mut y2 = coord2[1].parse::<i32>().unwrap();
            if y1 > y2 {
                let temp = y1;
                y1 = y2;
                y2 = temp;
            }
            for i in y1 .. y2 + 1 {
                let mut coord_key: String = coord1[0].to_string();
                coord_key.push_str(",");
                coord_key.push_str(&i.to_string());
                if points.contains_key(&coord_key) {
                    let cur_val = points.get(&coord_key).unwrap();
                    points.insert(coord_key, 1 + cur_val);
                } else {
                    points.insert(coord_key, 1);
                }
            }
        } else if coord1[1] == coord2[1] {
            let mut x1 = coord1[0].parse::<i32>().unwrap();
            let mut x2 = coord2[0].parse::<i32>().unwrap();
            if x1 > x2 {
                let temp = x1;
                x1 = x2;
                x2 = temp;
            }
            for i in x1 .. x2 + 1 {
                let mut coord_key: String = i.to_string();
                coord_key.push_str(",");
                coord_key.push_str(&coord1[1].to_string());
                if points.contains_key(&coord_key) {
                    let cur_val = points.get(&coord_key).unwrap();
                    points.insert(coord_key, 1 + cur_val);
                } else {
                    points.insert(coord_key, 1);
                }
            }
        }
    }

    let mut overlaps: i32 = 0;
    for (key, val) in points.iter() {
        if val >= &2 {
            overlaps += 1;
            println!("key: {} val: {}", key, val);
        }
    }

    println!("Overlaps: {:?}", overlaps);
}

pub fn run_day_5_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let _day5_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    // Determine how many entries there are and how many digits are needed for gamma
    // and epsilon
    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let mut points = HashMap::new();

    for line in input_values {
        let coords: Vec<&str> = line.split(" -> ").collect();
        let coord1: Vec<&str> = coords[0].split(",").collect();
        let coord2: Vec<&str> = coords[1].split(",").collect();
        let mut y1 = coord1[1].parse::<i32>().unwrap();
        let mut y2 = coord2[1].parse::<i32>().unwrap();
        let mut x1 = coord1[0].parse::<i32>().unwrap();
        let mut x2 = coord2[0].parse::<i32>().unwrap();
        // Vertical line with the same x coordinates
        if coord1[0] == coord2[0] {
            if y1 > y2 {
                let temp = y1;
                y1 = y2;
                y2 = temp;
            }
            for i in y1 .. y2 + 1 {
                let mut coord_key: String = coord1[0].to_string();
                coord_key.push_str(",");
                coord_key.push_str(&i.to_string());
                if points.contains_key(&coord_key) {
                    let cur_val = points.get(&coord_key).unwrap();
                    points.insert(coord_key, 1 + cur_val);
                } else {
                    points.insert(coord_key, 1);
                }
            }
        // Horizontal line with the same y coordinates
        } else if coord1[1] == coord2[1] {
            if x1 > x2 {
                let temp = x1;
                x1 = x2;
                x2 = temp;
            }
            for i in x1 .. x2 + 1 {
                let mut coord_key: String = i.to_string();
                coord_key.push_str(",");
                coord_key.push_str(&coord1[1].to_string());
                if points.contains_key(&coord_key) {
                    let cur_val = points.get(&coord_key).unwrap();
                    points.insert(coord_key, 1 + cur_val);
                } else {
                    points.insert(coord_key, 1);
                }
            }
        // Diagonal line
        } else {
            let mut x_mod = 0;
            let mut y_mod = 0;
            // Decrement both x and y
            if x1 > x2 && y1 > y2 {
                x_mod = -1;
                y_mod = -1;
            // Decrement x and increment y
            } else if x1 > x2 && y1 < y2 {
                x_mod = -1;
                y_mod = 1;
            // Increment x and decrement y
            }  else if x1 < x2 && y1 < y2 {
                x_mod = 1;
                y_mod = 1;
            // Increment both x and y
            }  else if x1 < x2 && y1 > y2 {
                x_mod = 1;
                y_mod = -1;
            }
            let length = (x1 - x2).abs();
            for i in 0 .. length + 1 {
                let cur_x = x1 + (i * x_mod);
                let cur_y = y1 + (i * y_mod);
                let mut coord_key: String = cur_x.to_string();
                coord_key.push_str(",");
                coord_key.push_str(&cur_y.to_string());
                if points.contains_key(&coord_key) {
                    let cur_val = points.get(&coord_key).unwrap();
                    points.insert(coord_key, 1 + cur_val);
                } else {
                    points.insert(coord_key, 1);
                }
            }
        }
    }

    let mut overlaps: i32 = 0;
    for (key, val) in points.iter() {
        if val >= &2 {
            overlaps += 1;
            println!("key: {} val: {}", key, val);
        }
    }

    println!("Overlaps: {:?}", overlaps);
}