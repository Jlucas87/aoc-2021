use std::fs;
use std::time::{Instant};

pub fn run_day_10_1(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let day10_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    // Iterate through each line and accumulate the sum of illegal characters
    let mut sum: i32 = 0;
    for line in input_values {
        // Create a stack to hold the delimiters as we iterate through each line
        let mut delimiters: Vec<&char> = Vec::new();
        let characters: Vec<char> = line.chars().collect();
        let mut top_char: &char = &'a';
        for character in &characters {
            // Push delimiter to stack
            let length = delimiters.len();
            match character {
                '(' => delimiters.push(&character),
                '[' => delimiters.push(&character),
                '{' => delimiters.push(&character),
                '<' => delimiters.push(&character),
                _ => {
                    top_char = delimiters.remove(length - 1);
                }
            }

            // Remove delimiter from stack and compare
            match character {
                ')' => {
                    if top_char != &'(' {
                        sum = sum + 3;
                    }
                },
                ']' => {
                    if top_char != &'[' {
                        sum = sum + 57;
                    }
                },
                '}' => {
                    if top_char != &'{' {
                        sum = sum + 1197;
                    }
                },
                '>' => {
                    if top_char != &'<' {
                        sum = sum + 25137;
                    }
                },
                _ => ()
            }
        }
    }

    println!("Total sum of discrepancies: {}", sum);
    println!("{}", format!("{}{}", "Microseconds: ", day10_1.elapsed().as_micros()));
}

pub fn run_day_10_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let day10_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let mut totals: Vec<i64> = Vec::new();

    // Iterate through each line and accumulate the sum of illegal characters
    for line in input_values {
        // Create a stack to hold the delimiters as we iterate through each line
        let mut delimiters: Vec<&char> = Vec::new();
        let characters: Vec<char> = line.chars().collect();
        let mut top_char: &char = &'a';
        let mut incomplete: bool = true;
        for character in &characters {
            // Push delimiter to stack
            let length = delimiters.len();
            match character {
                '(' => delimiters.push(&character),
                '[' => delimiters.push(&character),
                '{' => delimiters.push(&character),
                '<' => delimiters.push(&character),
                _ => {
                    top_char = delimiters.remove(length - 1);
                }
            }

            // Remove delimiter from stack and compare
            match character {
                ')' => {
                    if top_char != &'(' {
                        incomplete = false;
                    }
                },
                ']' => {
                    if top_char != &'[' {
                        incomplete = false;
                    }
                },
                '}' => {
                    if top_char != &'{' {
                        incomplete = false;
                    }
                },
                '>' => {
                    if top_char != &'<' {
                        incomplete = false;
                    }
                },
                _ => ()
            }
        }

        if incomplete {
            let mut total: i64 = 0;
            let mut length = delimiters.len();
            for _i in 0 .. length {
                total = total * 5;
                let delimiter = delimiters.remove(length - 1);
                match delimiter {
                    '(' => total = total + 1,
                    '[' => total = total + 2,
                    '{' => total = total + 3,
                    '<' => total = total + 4,
                    _ => ()
                }
                length = delimiters.len();
            }
            totals.push(total);
        }
    }

    totals.sort();
    println!("{}", totals.len());
    let desired_score: i64 = totals[((totals.len() + 1) / 2) - 1];
    println!("Grand list of totals {:?}", totals);
    println!("Desired score: {}", desired_score);
    println!("{}", format!("{}{}", "Microseconds: ", day10_1.elapsed().as_micros()));
}
