use std::fs;
use std::time::{Instant};
use std::collections::HashMap;
use std::collections::HashSet;

pub fn run_day_12_1(filename: String) {
    let _day12_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    // Keep track of all paths
    let mut paths: HashSet<String> = HashSet::new();

    // Create a map of the connections
    let mut connections: HashMap<&str, Vec<&str>> = HashMap::new();
    let mut available_nodes: HashSet<&str> = HashSet::new();
    for line in input_values {
        let line_arr: Vec<&str> = line.split("-").collect();
        available_nodes.insert(line_arr[1]);

        // Connect a - b
        if line_arr[1] != "start" && line_arr[0] != "end" {
            if !connections.contains_key(line_arr[0]) {
                let mut connection_vec: Vec<&str> = Vec::new();
                connection_vec.push(line_arr[1]);
                connections.insert(line_arr[0], connection_vec);
            } else {
                let mut connection_vec = connections.get(line_arr[0]).unwrap().to_vec();
                connection_vec.push(line_arr[1]);
                connections.insert(line_arr[0], connection_vec);
            }
        }

        // Connect b - a
        if line_arr[0] != "start" && line_arr[1] != "end" {
            if !connections.contains_key(line_arr[1]) {
                let mut connection_vec: Vec<&str> = Vec::new();
                connection_vec.push(line_arr[0]);
                connections.insert(line_arr[1], connection_vec);
            } else {
                let mut connection_vec = connections.get(line_arr[1]).unwrap().to_vec();
                connection_vec.push(line_arr[0]);
                connections.insert(line_arr[1], connection_vec);
            }
        }
    }

    // Create initial connections vec by fetching the start node
    let node_connections = connections.get("start").unwrap().to_vec();

    // Find all of the available nodes at the start
    let path = "start".to_owned();
    println!("Connections: {:?}", connections);

    paths = find_path (node_connections, available_nodes, path, paths, &connections);
    println!("Total paths found: {:?}", paths.len());
}

fn find_path (node_connections: Vec<&str>, available_nodes: HashSet<&str>, path: String, mut paths: HashSet<String>, connections: &HashMap<&str, Vec<&str>>) -> HashSet<String> {
    let mut path_found: bool = false;
    for node in node_connections {
        // Path found, follow additonal paths until reaching end
        if available_nodes.contains(node) {
            let new_path = format!("{}{}{}", path, "->".to_owned(), node);
            path_found = true;

            // Path found, add to paths vec
            if node == "end" {
                println!("Path found! {}", new_path);
                paths.insert(new_path);

            // Path continuing, continue navigating
            } else {
                // Create new available node list
                let mut new_available_nodes: HashSet<&str> = available_nodes.clone();
                // Remove if lowercase
                let upper_test_string = node.to_uppercase();
                if upper_test_string.as_str() != node {
                    new_available_nodes.remove(node);
                }

                // Create new node connections
                let new_node_connections: Vec<&str> = connections.get(node).unwrap().to_vec();
                paths = find_path(new_node_connections, new_available_nodes, new_path, paths, &connections);
            }
        }
    }

    if !path_found {
        println!("Dead end, path: {}", path);
    }

    return paths;
}

pub fn run_day_12_2(filename: String) {
    let _day12_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    // Keep track of all paths
    let mut paths: HashSet<String> = HashSet::new();

    // Create a map of the connections
    let mut connections: HashMap<&str, Vec<&str>> = HashMap::new();
    let mut available_nodes: HashSet<&str> = HashSet::new();
    for line in input_values {
        let line_arr: Vec<&str> = line.split("-").collect();
        available_nodes.insert(line_arr[1]);
        available_nodes.insert(line_arr[0]);

        // Connect a - b
        if line_arr[1] != "start" && line_arr[0] != "end" {
            if !connections.contains_key(line_arr[0]) {
                let mut connection_vec: Vec<&str> = Vec::new();
                connection_vec.push(line_arr[1]);
                connections.insert(line_arr[0], connection_vec);
            } else {
                let mut connection_vec = connections.get(line_arr[0]).unwrap().to_vec();
                connection_vec.push(line_arr[1]);
                connections.insert(line_arr[0], connection_vec);
            }
        }

        // Connect b - a
        if line_arr[0] != "start" && line_arr[1] != "end" {
            if !connections.contains_key(line_arr[1]) {
                let mut connection_vec: Vec<&str> = Vec::new();
                connection_vec.push(line_arr[0]);
                connections.insert(line_arr[1], connection_vec);
            } else {
                let mut connection_vec = connections.get(line_arr[1]).unwrap().to_vec();
                connection_vec.push(line_arr[0]);
                connections.insert(line_arr[1], connection_vec);
            }
        }
    }

    // Create initial connections vec by fetching the start node
    let node_connections = connections.get("start").unwrap().to_vec();

    // Find all of the available nodes at the start
    let path = "start".to_owned();
    println!("Connections: {:?}", available_nodes);

    find_path_small_cave_2x (node_connections, available_nodes, path, &mut paths, &connections, false);
    println!("Total paths found: {:?}", paths.len());
}

fn find_path_small_cave_2x (node_connections: Vec<&str>, available_nodes: HashSet<&str>, path: String, mut paths: &mut HashSet<String>,
    connections: &HashMap<&str, Vec<&str>>, two_found: bool) {
    let mut path_found: bool = false;
    for node in node_connections {
        // Path found, follow additonal paths until reaching end
        let upper_test_string = node.to_uppercase();
        let mut new_available_nodes: HashSet<&str> = available_nodes.clone();
        if two_found == true && upper_test_string != node && path.contains(node) {
            new_available_nodes.remove(node);
        }

        if new_available_nodes.contains(node) {
            let new_path = format!("{}{}{}", path, "->".to_owned(), node);
            path_found = true;

            // Path found, add to paths vec
            if node == "end" {
                println!("Path found! {}", new_path);
                paths.insert(new_path);

            // Path continuing, continuing navigating
            } else {
                // Create new available node list
                let mut new_two_found: bool = two_found.clone();
                // Remove if lowercase
                let upper_test_string = node.to_uppercase();
                if upper_test_string.as_str() != node {
                    if path.contains(node) {
                        new_two_found = true;
                        new_available_nodes.remove(node);
                    }
                }

                // Create new node connections
                let new_node_connections: Vec<&str> = connections.get(node).unwrap().to_vec();
                find_path_small_cave_2x(new_node_connections, new_available_nodes, new_path, &mut paths, &connections, new_two_found);
            }
        }
    }

    if !path_found {
        println!("Dead end, path: {}", path);
    }
}