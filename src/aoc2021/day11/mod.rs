use std::fs;
use std::time::{Instant};

pub fn run_day_11_1(filename: String, steps: i32) {
    let _depth = 0;
    let _distance = 0;
    let day10_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    // Create the octopus grid
    let mut octopi: Vec<Vec<i32>> = Vec::new();
    for (_i, line) in input_values.iter().enumerate() {
        let chars: Vec<i32> = line.split_terminator("")
            .skip(1)
            .map(|x| { x.parse().unwrap() })
            .collect();
        octopi.push(chars);
    }

    let mut flashes: i32 = 0;
    for i in 0 .. steps {
        println!("Step: {}", i);
        // Increment all of the octopi and keep track of which are flashing
        let mut flashing_octopi: Vec<String> = Vec::new();
        for x in 0 .. octopi.len() {
            for y in 0 .. octopi[x].len() {
                // Iterate self and check for flash
                octopi[x][y] = octopi[x][y] + 1;
                if octopi[x][y] <= 9 {
                    continue;
                } else {
                    octopi[x][y] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            }
        }

        // Iterate through flashing octopi until none remain
        while flashing_octopi.len() > 0 {
            let cur_octopus = flashing_octopi.pop().unwrap();
            let coord_split: Vec<&str> = cur_octopus.split(",").collect();
            let x: usize = coord_split[0].parse::<usize>().unwrap();
            let y: usize = coord_split[1].parse::<usize>().unwrap();

            // Check value above
            if x != 0 {
                let above = octopi[x - 1][y];
                if above < 9 && above > 0 {
                    octopi[x - 1][y] = octopi[x - 1][y] + 1;
                } else if above == 9 {
                    octopi[x - 1][y] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), y.to_string()));
                }
            }

            // Check value above-right
            if x != 0 && y < octopi[x].len() - 1  {
                let above_right = octopi[x - 1][y + 1];
                if above_right < 9 && above_right > 0 {
                    octopi[x - 1][y + 1] = octopi[x - 1][y + 1] + 1;
                } else if above_right == 9 {
                    octopi[x - 1][y + 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value to the right
            if y < octopi[x].len() - 1  {
                let right = octopi[x][y + 1];
                if right < 9 && right > 0 {
                    octopi[x][y + 1] = octopi[x][y + 1] + 1;
                } else if right == 9 {
                    octopi[x][y + 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value below-right
            if x != octopi.len() - 1 && y < octopi[x].len() - 1  {
                let below_right = octopi[x + 1][y + 1];
                if below_right < 9 && below_right > 0 {
                    octopi[x + 1][y + 1] = octopi[x + 1][y + 1] + 1;
                } else if below_right == 9 {
                    octopi[x + 1][y + 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value below
            if x != octopi.len() - 1 {
                let below = octopi[x + 1][y];
                if below < 9 && below > 0 {
                    octopi[x + 1][y] = octopi[x + 1][y] + 1;
                } else if below == 9 {
                    octopi[x + 1][y] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), y.to_string()));
                }
            }

            // Check value below-left
            if x != octopi.len() - 1 && y != 0 {
                let below_left = octopi[x + 1][y - 1];
                if below_left < 9 && below_left > 0 {
                    octopi[x + 1][y - 1] = octopi[x + 1][y - 1] + 1;
                } else if below_left == 9 {
                    octopi[x + 1][y - 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }

            // Check value left
            if y != 0 {
                let left = octopi[x][y - 1];
                if left < 9 && left > 0 {
                    octopi[x][y - 1] = octopi[x][y - 1] + 1;
                } else if left == 9 {
                    octopi[x][y - 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }

            // Check value above-left
            if y != 0 && x != 0 {
                let above_left = octopi[x - 1][y - 1];
                if above_left < 9 && above_left > 0 {
                    octopi[x - 1][y - 1] = octopi[x - 1][y - 1] + 1;
                } else if above_left == 9 {
                    octopi[x - 1][y - 1] = 0;
                    flashes += 1;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }
        }

        println!("{:?}", octopi);
    }

    println!("Flashes: {}", flashes);
    println!("{}", format!("{}{}", "Microseconds: ", day10_1.elapsed().as_micros()));
}

pub fn run_day_11_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let day10_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    // Create the octopus grid
    let mut octopi: Vec<Vec<i32>> = Vec::new();
    for (_i, line) in input_values.iter().enumerate() {
        let chars: Vec<i32> = line.split_terminator("")
            .skip(1)
            .map(|x| { x.parse().unwrap() })
            .collect();
        octopi.push(chars);
    }

    let mut keep_flashing: bool = true;
    let mut i: i32 = 0;
    while keep_flashing {
        i += 1;
        // Increment all of the octopi and keep track of which are flashing
        let mut flashing_octopi: Vec<String> = Vec::new();
        for x in 0 .. octopi.len() {
            for y in 0 .. octopi[x].len() {
                // Iterate self and check for flash
                octopi[x][y] = octopi[x][y] + 1;
                if octopi[x][y] <= 9 {
                    continue;
                } else {
                    octopi[x][y] = 0;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            }
        }

        // Iterate through flashing octopi until none remain
        while flashing_octopi.len() > 0 {
            let cur_octopus = flashing_octopi.pop().unwrap();
            let coord_split: Vec<&str> = cur_octopus.split(",").collect();
            let x: usize = coord_split[0].parse::<usize>().unwrap();
            let y: usize = coord_split[1].parse::<usize>().unwrap();

            // Check value above
            if x != 0 {
                let above = octopi[x - 1][y];
                if above < 9 && above > 0 {
                    octopi[x - 1][y] = octopi[x - 1][y] + 1;
                } else if above == 9 {
                    octopi[x - 1][y] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), y.to_string()));
                }
            }

            // Check value above-right
            if x != 0 && y < octopi[x].len() - 1  {
                let above_right = octopi[x - 1][y + 1];
                if above_right < 9 && above_right > 0 {
                    octopi[x - 1][y + 1] = octopi[x - 1][y + 1] + 1;
                } else if above_right == 9 {
                    octopi[x - 1][y + 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value to the right
            if y < octopi[x].len() - 1  {
                let right = octopi[x][y + 1];
                if right < 9 && right > 0 {
                    octopi[x][y + 1] = octopi[x][y + 1] + 1;
                } else if right == 9 {
                    octopi[x][y + 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value below-right
            if x != octopi.len() - 1 && y < octopi[x].len() - 1  {
                let below_right = octopi[x + 1][y + 1];
                if below_right < 9 && below_right > 0 {
                    octopi[x + 1][y + 1] = octopi[x + 1][y + 1] + 1;
                } else if below_right == 9 {
                    octopi[x + 1][y + 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), (y + 1).to_string()));
                }
            }

            // Check value below
            if x != octopi.len() - 1 {
                let below = octopi[x + 1][y];
                if below < 9 && below > 0 {
                    octopi[x + 1][y] = octopi[x + 1][y] + 1;
                } else if below == 9 {
                    octopi[x + 1][y] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), y.to_string()));
                }
            }

            // Check value below-left
            if x != octopi.len() - 1 && y != 0 {
                let below_left = octopi[x + 1][y - 1];
                if below_left < 9 && below_left > 0 {
                    octopi[x + 1][y - 1] = octopi[x + 1][y - 1] + 1;
                } else if below_left == 9 {
                    octopi[x + 1][y - 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }

            // Check value left
            if y != 0 {
                let left = octopi[x][y - 1];
                if left < 9 && left > 0 {
                    octopi[x][y - 1] = octopi[x][y - 1] + 1;
                } else if left == 9 {
                    octopi[x][y - 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", x.to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }

            // Check value above-left
            if y != 0 && x != 0 {
                let above_left = octopi[x - 1][y - 1];
                if above_left < 9 && above_left > 0 {
                    octopi[x - 1][y - 1] = octopi[x - 1][y - 1] + 1;
                } else if above_left == 9 {
                    octopi[x - 1][y - 1] = 0;
                    flashing_octopi.push(format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), (y - 1).to_string()));
                }
            }
        }

        let mut all_zero: bool = true;
        for x in 0 .. octopi.len() {
            for y in 0 .. octopi[x].len() {
                if octopi[x][y] != 0 {
                    all_zero = false;
                }
            }
        }

        if all_zero == true {
            keep_flashing = false;
        }
    }

    println!("Steps required for all of them to flash together: {}", i);
    println!("{}", format!("{}{}", "Microseconds: ", day10_1.elapsed().as_micros()));
}
