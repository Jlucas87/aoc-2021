use std::fs;
use std::time::{Instant};
use regex::Regex;

pub fn run_day_4_1(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let day4_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let matches = vec![
        Regex::new("x{5}.{20}").unwrap(),
        Regex::new(".{5}x{5}.{15}").unwrap(),
        Regex::new(".{10}x{5}.{10}").unwrap(),
        Regex::new(".{15}x{5}.{5}").unwrap(),
        Regex::new(".{20}x{5}").unwrap(),
        Regex::new("x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}").unwrap(),
        Regex::new(".{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}").unwrap(),
        Regex::new(".{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}").unwrap(),
        Regex::new(".{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}").unwrap(),
        Regex::new(".{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}").unwrap()
    ];

    let initial_state = String::from("0000000000000000000000000");

    // Collect the list of data and fetch the bingo numbers
    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let length: i32 = input_values.len() as i32;
    println!("{:?}", length);
    let bingo_numbers: Vec<&str> = input_values[0].split(",").collect();
    let mut bingo_boards: Vec<Vec<Vec<&str>>> = Vec::new();

    // Create the bingo boards vector
    let mut current_board = 99999;
    for i in 1 .. length {
        if input_values[i as usize] == "" {
            bingo_boards.push(Vec::new());
            if current_board == 99999 {
                current_board = 0
            } else {
                current_board += 1;
            }
        } else {
            let split_string: Vec<&str> = input_values[i as usize].split_whitespace().collect();
            bingo_boards[current_board].push(split_string);
        }
    }
    let mut board_states = vec![initial_state; bingo_boards.len()];

    // Start iterating through the bingo numbers
    let mut match_found = false;
    for num in bingo_numbers {
        for b in 0 .. bingo_boards.len() {
            for (r, row) in bingo_boards[b as usize].iter().enumerate() {
                for j in 0 .. row.len() {
                    if row[j as usize] == num {
                        /*
                        * The current board position is: i.
                        * The current row is: r.
                        * The current item is: j.
                        */
                        board_states[b].replace_range((r * 5) + j .. (r * 5) + j + 1 , "x");
                        let result = check_for_match(&matches, &board_states[b]);
                        if result == true {
                            match_found = true;
                            println!("Matching board: {}", b);
                            println!("Board state: {}", board_states[b]);
                            println!("Board: {:?}", bingo_boards[b]);
                            println!("Current number: {}", num);
                            let mut sum = 0;
                            for (index, c) in board_states[b].chars().enumerate() {
                                if c == '0' {
                                    sum += bingo_boards[b as usize][index/5 as usize][index % 5 as usize].parse::<i32>().unwrap();
                                }
                            }
                            println!("Current number: {}", sum);
                            println!("Final solution: {} * {} = {}", sum, num, sum * num.parse::<i32>().unwrap());
                            break;
                        }
                    }
                }
                if match_found {
                    break;
                }
            }
            if match_found {
                break;
            }
        }
        if match_found {
            break;
        }
    }
    println!("{}", format!("{}{}", "Microseconds: ", day4_1.elapsed().as_micros()));
}

fn check_for_match(matches: &Vec<Regex>, check: &str) -> bool {
    let mut bingo = false;
    for item in matches {
        if item.is_match(check) {
            bingo = true;
            break;
        }
    }

    return bingo;
}

pub fn run_day_4_2(filename: String) {
    let _depth = 0;
    let _distance = 0;
    let day4_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let matches = vec![
        Regex::new("x{5}.{20}").unwrap(),
        Regex::new(".{5}x{5}.{15}").unwrap(),
        Regex::new(".{10}x{5}.{10}").unwrap(),
        Regex::new(".{15}x{5}.{5}").unwrap(),
        Regex::new(".{20}x{5}").unwrap(),
        Regex::new("x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}").unwrap(),
        Regex::new(".{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}.{1}x{1}.{3}").unwrap(),
        Regex::new(".{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}.{2}x{1}.{2}").unwrap(),
        Regex::new(".{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}.{3}x{1}.{1}").unwrap(),
        Regex::new(".{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}.{4}x{1}").unwrap()
    ];

    let initial_state = String::from("0000000000000000000000000");

    // Collect the list of data and fetch the bingo numbers
    let input_values: Vec<&str> = contents.split("\n")
        .collect();
    let length: i32 = input_values.len() as i32;
    println!("{:?}", length);
    let bingo_numbers: Vec<&str> = input_values[0].split(",").collect();
    let mut bingo_boards: Vec<Vec<Vec<&str>>> = Vec::new();

    // Create the bingo boards vector
    let mut current_board = 99999;
    for i in 1 .. length {
        if input_values[i as usize] == "" {
            bingo_boards.push(Vec::new());
            if current_board == 99999 {
                current_board = 0
            } else {
                current_board += 1;
            }
        } else {
            let split_string: Vec<&str> = input_values[i as usize].split_whitespace().collect();
            bingo_boards[current_board].push(split_string);
        }
    }
    let mut board_states = vec![initial_state; bingo_boards.len()];
    let mut winning_boards: Vec<i32> = Vec::new();

    // Start iterating through the bingo numbers
    let mut match_found = false;
    for num in bingo_numbers {
        for b in 0 .. bingo_boards.len() {
            for (r, row) in bingo_boards[b as usize].iter().enumerate() {
                for j in 0 .. row.len() {
                    if row[j as usize] == num && !winning_boards.contains(&(*&b as i32)) {
                        /*
                        * The current board position is: i.
                        * The current row is: r.
                        * The current item is: j.
                        */
                        board_states[b].replace_range((r * 5) + j .. (r * 5) + j + 1 , "x");
                        let result = check_for_match(&matches, &board_states[b]);
                        if result == true {
                            match_found = true;
                            println!("Matching board: {}", b);
                            println!("Board state: {}", board_states[b]);
                            println!("Board: {:?}", bingo_boards[b]);
                            println!("Current number: {}", num);
                            let mut sum = 0;
                            for (index, c) in board_states[b].chars().enumerate() {
                                if c == '0' {
                                    sum += bingo_boards[b as usize][index/5 as usize][index % 5 as usize].parse::<i32>().unwrap();
                                }
                            }
                            println!("Current sum: {}", sum);
                            println!("Final solution: {} * {} = {}", sum, num, sum * num.parse::<i32>().unwrap());
                            winning_boards.push(b as i32);
                        }
                    }
                }
            }
        }
    }
    println!("{}", format!("{}{}", "Microseconds: ", day4_1.elapsed().as_micros()));
}