use std::fs;
use std::time::{Instant};

pub fn run_day_2_1(filename: String) {
    let mut depth = 0;
    let mut distance = 0;
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    for item in input_values {
        let item_arr: Vec<&str> = item.split(" ").collect();
        let direction = item_arr[0];
        let amount: i32 = item_arr[1].parse().unwrap();

        match direction {
            "forward" => distance += amount,
            "up" => depth -= amount,
            "down" => depth += amount,
            _ => println!("Input does not equal any value"),
        }
    }

    println!("depth: {}, distance: {}", depth, distance);
    println!("Total: {}", depth * distance);
}

pub fn run_day_2_2(filename: String) {
    let mut depth = 0;
    let mut distance = 0;
    let mut aim = 0;
    let _day1_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values: Vec<&str> = contents.split("\n")
        .collect();

    for item in input_values {
        let item_arr: Vec<&str> = item.split(" ").collect();
        let direction = item_arr[0];
        let amount: i32 = item_arr[1].parse().unwrap();

        match direction {
            "forward" => {
                distance += amount;
                depth = depth + (aim * amount);
            },
            "up" => aim -= amount,
            "down" => aim += amount,
            _ => println!("Input does not equal any value"),
        }
        println!("{}", aim);
    }

    println!("depth: {}, distance: {}", depth, distance);
    println!("Total: {}", depth * distance);
}
