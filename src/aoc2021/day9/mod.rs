use std::fs;
use std::time::{Instant};


pub fn run_day_9_1(filename: String) {
    let day9_1 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let lines: Vec<&str> = contents.split("\n")
         .collect();
    let mut coords: Vec<Vec<&str>> = Vec::new();
    let _risk_level: i32 =  0;

    for (_i, line) in lines.iter().enumerate() {
        let chars: Vec<&str> = line.split_terminator("").skip(1).collect();
        coords.push(chars);
    }

    let risk_level = find_risk_level(coords);

    println!("Risk level is: {}", risk_level);
    println!("{}", format!("{}{}", "Microseconds: ", day9_1.elapsed().as_micros()));
}

pub fn run_day_9_2(filename: String) {
    let day9_2 = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let lines: Vec<&str> = contents.split("\n")
         .collect();
    let mut coords: Vec<Vec<&str>> = Vec::new();

    for (_i, line) in lines.iter().enumerate() {
        let chars: Vec<&str> = line.split_terminator("").skip(1).collect();
        coords.push(chars);
    }
    let mut low_points: Vec<String> = Vec::new();

    for (x, row) in coords.iter().enumerate() {
        for (y, _col) in row.iter().enumerate() {
            let current: i32 = coords[x][y].parse::<i32>().unwrap();
            let mut above: i32 = 0;
            let mut below: i32 = 0;
            let mut left: i32 = 0;
            let mut right: i32 = 0;
            if x > 0 {
                above = coords[x - 1][y].parse::<i32>().unwrap();
            }
            if y > 0 {
                left = coords[x][y - 1].parse::<i32>().unwrap();
            }
            if x < coords.len() - 1 {
                below = coords[x + 1][y].parse::<i32>().unwrap();
            }
            if y < row.len() - 1 {
                right = coords[x][y + 1].parse::<i32>().unwrap();
            }

            // Check below and to the right
            if x == 0 && y == 0 {
                if current < below && current < right {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            }
            // Check left and below
            else if x == 0 && y == row.len() - 1 {
                if current < below && current < left {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            }
            // Check below, left and right
            else if x == 0 {
                if current < below && current < right && current < left {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check right and above
            } else if x == coords.len() - 1 && y == 0 {
                if current < above && current < right {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check above, below and right
            } else if y == 0 {
                if current < above && current < right && current < below {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check above and to the left
            } else if x == coords.len() - 1 && y == row.len() - 1 {
                if current < above && current < left {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check above, left and right
            } else if x == coords.len() - 1 {
                if current < above && current < right && current < left {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check above, below, and left
            } else if y == row.len() - 1 {
                if current < above && current < below && current < left {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            // Check all directions
            } else {
                if current < above && current < below && current < left && current < right {
                    println!("Low point found: {}", current);
                    low_points.push(format!("{}{}{}", x.to_string(), ",".to_owned(), y.to_string()));
                }
            }
        }
    }
    let mut basins: Vec<i32> = Vec::new();
    for coord in low_points {
        let mut basin: Vec<String> = Vec::new();
        let mut processed: Vec<String> = Vec::new();
        basin.push(coord);
        while basin.len() > 0 {
            // Add current string to processed list
            let cur: String = basin.remove(0);
            processed.push(String::from(&cur));
            let coord_split: Vec<&str> = cur.split(",").collect();

            // Add items to the queue as needed
            basin = add_items_to_queue(coord_split[0].parse::<usize>().unwrap(), coord_split[1].parse::<usize>().unwrap(), basin, &coords, &processed);
        }
        basins.push(processed.len() as i32);
    }
    basins.sort_by(|a, b| b.cmp(a));
    println!("Basin sizes are as follows: {:?}", basins);
    println!("Total size: {:?}", basins[0] * basins[1] * basins[2]);
    println!("{}", format!("{}{}", "Microseconds: ", day9_2.elapsed().as_micros()));
}

fn add_items_to_queue (x: usize, y: usize, mut queue: Vec<String>, coords: &Vec<Vec<&str>>, processed: &Vec<String>) -> Vec<String> {
    let mut above: i32 = 10;
    let mut below: i32 = 10;
    let mut left: i32 = 10;
    let mut right: i32 = 10;
    if x > 0 {
        above = coords[x - 1][y].parse::<i32>().unwrap();
    }
    if y > 0 {
        left = coords[x][y - 1].parse::<i32>().unwrap();
    }
    if x < coords.len() - 1 {
        below = coords[x + 1][y].parse::<i32>().unwrap();
    }
    if y < coords[0].len() - 1 {
        right = coords[x][y + 1].parse::<i32>().unwrap();
    }

    if above < 9 {
        let above_string = format!("{}{}{}", (x - 1).to_string(), ",".to_owned(), y.to_string());
        if !processed.contains(&above_string) && !queue.contains(&above_string) {
            queue.push(above_string);
        }
    }
    if below < 9 {
        let below_string = format!("{}{}{}", (x + 1).to_string(), ",".to_owned(), y.to_string());
        if !processed.contains(&below_string) && !queue.contains(&below_string) {
            queue.push(below_string);
        }
    }
    if left < 9 {
        let left_string = format!("{}{}{}", x.to_string(), ",".to_owned(), (y - 1).to_string());
        if !processed.contains(&left_string) && !queue.contains(&left_string) {
            queue.push(left_string);
        }
    }

    if right < 9 {
        let right_string = format!("{}{}{}", x.to_string(), ",".to_owned(), (y + 1).to_string());
        if !processed.contains(&right_string) && !queue.contains(&right_string) {
            queue.push(right_string);
        }
    }

    return queue;
}

fn find_risk_level (coords: Vec<Vec<&str>>) -> i32 {
    let mut risk_level: i32 = 0;
    for (x, row) in coords.iter().enumerate() {
        for (y, _col) in row.iter().enumerate() {
            let current: i32 = coords[x][y].parse::<i32>().unwrap();
            let mut above: i32 = 0;
            let mut below: i32 = 0;
            let mut left: i32 = 0;
            let mut right: i32 = 0;
            if x > 0 {
                above = coords[x - 1][y].parse::<i32>().unwrap();
            }
            if y > 0 {
                left = coords[x][y - 1].parse::<i32>().unwrap();
            }
            if x < coords.len() - 1 {
                below = coords[x + 1][y].parse::<i32>().unwrap();
            }
            if y < row.len() - 1 {
                right = coords[x][y + 1].parse::<i32>().unwrap();
            }

            // Check below and to the right
            if x == 0 && y == 0 {
                if current < below && current < right {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            }
            // Check left and below
            else if x == 0 && y == row.len() - 1 {
                if current < below && current < left {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            }
            // Check below, left and right
            else if x == 0 {
                if current < below && current < right && current < left {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check right and above
            } else if x == coords.len() - 1 && y == 0 {
                if current < above && current < right {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check above, below and right
            } else if y == 0 {
                if current < above && current < right && current < below {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check above and to the left
            } else if x == coords.len() - 1 && y == row.len() - 1 {
                if current < above && current < left {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check above, left and right
            } else if x == coords.len() - 1 {
                if current < above && current < right && current < left {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check above, below, and left
            } else if y == row.len() - 1 {
                if current < above && current < below && current < left {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            // Check all directions
            } else {
                if current < above && current < below && current < left && current < right {
                    println!("Low point found: {}", current);
                    risk_level = risk_level + 1 + current;
                }
            }
        }
    }

    return risk_level;
}
