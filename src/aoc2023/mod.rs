mod day1;
mod day2;

pub fn run_days() {
    // Day 1
    // println!("Day 1, part1");
    // day1::run_day_1_1(String::from("./src/aoc2023/day1/input.txt"));
    
    // println!("Day 1, part2");
    // day1::run_day_1_2(String::from("./src/aoc2023/day1/input.txt"));

    // Day 2
    println!("Day 2, part1");
    day2::run_day_2_1(String::from("./src/aoc2023/day2/input.txt"));
    
    println!("Day 2, part2");
    day2::run_day_2_2(String::from("./src/aoc2023/day2/input.txt"));
}
