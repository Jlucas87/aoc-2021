use std::fs;
use std::time::{Instant};

pub fn run_day_2_1(filename: String) {
    let day2_1_start = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values = contents.split("\n");
    const RED_CUBES:usize = 12;
    const GREEN_CUBES:usize = 13;
    const BLUE_CUBES:usize = 14;
    let mut sum = 0;

    // Iterate through each row looking for impossible games
    let mut game_index = 0;
    for row in input_values {
      // Split up each row by each game
      game_index += 1;
      let game_list:Vec<&str> = row.split(":").collect();
      let games = game_list[1].split(";");
      let mut impossible = false;
      
      // Evaluate each cube pull for violations
      for game in games {
        let cube_list = game.split(",");
        for cube in cube_list {
          if cube.find("red") != None {
            let cube_data:Vec<&str> = cube.split(" ").collect();
            if cube_data[1].parse::<usize>().unwrap() > RED_CUBES {
              impossible = true;
            }
          }
          if cube.find("green") != None {
            let cube_data:Vec<&str> = cube.split(" ").collect();
            if cube_data[1].parse::<usize>().unwrap() > GREEN_CUBES {
              impossible = true;
            }
          }
          if cube.find("blue") != None {
            let cube_data:Vec<&str> = cube.split(" ").collect();
            if cube_data[1].parse::<usize>().unwrap() > BLUE_CUBES {
              impossible = true;
            }
          }
        }

        if (impossible) {
          break;
        }
      }

      if (!impossible) {
        sum += game_index;
      }
    }
    
    println!("Result: {}", sum);
    println!("Time taken ms: {}", day2_1_start.elapsed().as_millis());
}

pub fn run_day_2_2(filename: String) {
  let day2_2_start = Instant::now();
  let contents = fs::read_to_string(filename)
      .expect("Something went wrong reading the file");

  let input_values = contents.split("\n");
  const RED_CUBES:usize = 12;
  const GREEN_CUBES:usize = 13;
  const BLUE_CUBES:usize = 14;
  let mut sum = 0;

  // Iterate through each row looking for impossible games
  let mut sum = 0;
  for row in input_values {
    // Split up each row by each game
    let game_list:Vec<&str> = row.split(":").collect();
    let games = game_list[1].split(";");
    let mut red_max:usize = 0;
    let mut green_max:usize = 0; 
    let mut blue_max:usize = 0; 
    
    // Evaluate each cube pull for violations
    for game in games {
      let cube_list = game.split(",");
      for cube in cube_list {
        if cube.find("red") != None {
          let cube_data:Vec<&str> = cube.split(" ").collect();
          let current_red = cube_data[1].parse::<usize>().unwrap();
          if current_red > red_max {
            red_max = current_red; 
          }
        }
        if cube.find("green") != None {
          let cube_data:Vec<&str> = cube.split(" ").collect();
          let current_green = cube_data[1].parse::<usize>().unwrap();
          if current_green > green_max {
            green_max = current_green; 
          }
        }
        if cube.find("blue") != None {
          let cube_data:Vec<&str> = cube.split(" ").collect();
          let current_blue = cube_data[1].parse::<usize>().unwrap();
          if current_blue > blue_max {
            blue_max = current_blue;
          }
        }
      }
    }

    let cube_power = red_max * green_max * blue_max;
    sum += cube_power;
  }
  
  println!("Result: {}", sum);
  println!("Time taken ms: {}", day2_2_start.elapsed().as_millis());
}

