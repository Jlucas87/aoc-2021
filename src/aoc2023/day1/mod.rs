use std::fs;
use std::time::{Instant};

pub fn run_day_1_1(filename: String) {
    let day1_1_start = Instant::now();
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let input_values = contents.split("\n");
    let mut sum: u32 = 0;
    
    for row in input_values {
      // Assign the first interger to a value
      let mut calibration: String = String::new();
      for c in row.chars() { 
        if c.is_numeric() {
          calibration.push(c);
          break;
        }
      }

      // Assign the second integer to a value
      for c in row.chars().rev().collect::<String>().chars() { 
        if c.is_numeric() {
          calibration.push(c);
          break;
        }
      }
      sum += calibration.parse::<u32>().unwrap();
    }

    println!("{}", sum);
    println!("{}", "Total time taken: ");
    println!("{}", day1_1_start.elapsed().as_millis());
}

pub fn run_day_1_2(filename: String) {
  let day1_2_start = Instant::now();
  let contents = fs::read_to_string(filename)
      .expect("Something went wrong reading the file");

  let input_values = contents.split("\n");
  let mut sum: u32 = 0;
  let word_numbers: [&str; 9] = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
  let numbers: [&str; 9] = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];

  for row in input_values {
    // Assign the first interger to a value
    let mut lowest_index = 9999;
    let mut lowest_value = 10;
    let mut highest_index = 99; 
    let mut highest_value = 99;
    for (i, num) in word_numbers.iter().enumerate() {
      let index = row.find(num);
      let last_index = row.rfind(num);
      if (index != None) {
        if (index.unwrap() < lowest_index) {
          lowest_index = index.unwrap();
          lowest_value = i + 1;
        }

        if (last_index.unwrap() > highest_index || highest_index == 99) {
          highest_index = last_index.unwrap();
          highest_value = i + 1;
        }
      }
    }

    for (i, num) in numbers.iter().enumerate() {
      let index = row.find(num);
      let last_index = row.rfind(num);
      if (index != None) {
        if (index.unwrap() < lowest_index) {
          lowest_index = index.unwrap();
          lowest_value = i + 1;
        }

        if (last_index.unwrap() > highest_index || highest_index == 99) {
          highest_index = last_index.unwrap();
          highest_value = i + 1;
        }
      }
    }
    let value = lowest_value.to_string() + &highest_value.to_string();
    sum += value.parse::<u32>().unwrap();
  }

  println!("{}", sum);
  println!("{}", "Total time taken: ");
  println!("{}", day1_2_start.elapsed().as_millis());
}


